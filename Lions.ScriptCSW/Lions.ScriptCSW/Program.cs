﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Lions.ScriptCSW
{
    public class Program
    {
        public static int Main(string[] args)
        {
            var process = new Process();
            
            var startInfo = process.StartInfo;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;

            startInfo.FileName = @"scriptcs.exe";
            startInfo.Arguments = GetArgumentsString();
            
            process.Start();

            process.WaitForExit();

            return process.ExitCode;
        }

        private static string GetArgumentsString()
        {
            var originalCommandLine = Environment.CommandLine;

            var programAndArguments = originalCommandLine.Split(new[] {' '}, 2);
            return programAndArguments.Length == 2 ? programAndArguments[1] : string.Empty;
        }
    }
}
