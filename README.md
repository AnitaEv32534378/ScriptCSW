# Overview
ScriptCSW is a wrapper application that allows to execute [ScriptCS](http://scriptcs.net/) scripts without associated console window.

# Installation Instructions
1. Download the latest release from this page: https://gitlab.com/Lions-Open-Source/ScriptCSW/tags/ScriptCSW_1.0.0.20160606
2. Unzip files to a folder where scriptcs is installed.  
Hint: In order to find location of scriptcs use the following command:  
``` where scriptcs ```  
Default location of scriptcs folder is **C:\ProgramData\chocolatey\bin\scriptcs.exe**.

# Credits
* [alexvovk.relbis.com](http://alexvovk.relbis.com)